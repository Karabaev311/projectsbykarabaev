﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Exit1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OpenForm11ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStripStatusLabel2.Text = "Открытие формы 1";
            Form2 NewForm = new Form2();
            NewForm.MdiParent = this;
            NewForm.Show();
        }


    }
}
