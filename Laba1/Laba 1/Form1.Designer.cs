﻿namespace Laba_1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.File1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Forms1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenForm11ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenForm21ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar2 = new System.Windows.Forms.ToolStripProgressBar();
            this.menuStrip2.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File1ToolStripMenuItem,
            this.Forms1ToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(424, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // File1ToolStripMenuItem
            // 
            this.File1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Exit1ToolStripMenuItem});
            this.File1ToolStripMenuItem.Name = "File1ToolStripMenuItem";
            this.File1ToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.File1ToolStripMenuItem.Text = "Файл";
            // 
            // Exit1ToolStripMenuItem
            // 
            this.Exit1ToolStripMenuItem.Name = "Exit1ToolStripMenuItem";
            this.Exit1ToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.Exit1ToolStripMenuItem.Text = "Выход";
            this.Exit1ToolStripMenuItem.Click += new System.EventHandler(this.Exit1ToolStripMenuItem_Click);
            // 
            // Forms1ToolStripMenuItem
            // 
            this.Forms1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenForm11ToolStripMenuItem,
            this.OpenForm21ToolStripMenuItem});
            this.Forms1ToolStripMenuItem.Name = "Forms1ToolStripMenuItem";
            this.Forms1ToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.Forms1ToolStripMenuItem.Text = "Формы";
            // 
            // OpenForm11ToolStripMenuItem
            // 
            this.OpenForm11ToolStripMenuItem.Name = "OpenForm11ToolStripMenuItem";
            this.OpenForm11ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.OpenForm11ToolStripMenuItem.Text = "Открыть форму1";
            this.OpenForm11ToolStripMenuItem.Click += new System.EventHandler(this.OpenForm11ToolStripMenuItem_Click);
            // 
            // OpenForm21ToolStripMenuItem
            // 
            this.OpenForm21ToolStripMenuItem.Name = "OpenForm21ToolStripMenuItem";
            this.OpenForm21ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.OpenForm21ToolStripMenuItem.Text = "Открыть фому2";
            // 
            // statusStrip2
            // 
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.toolStripProgressBar2});
            this.statusStrip2.Location = new System.Drawing.Point(0, 309);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(424, 22);
            this.statusStrip2.TabIndex = 1;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(124, 17);
            this.toolStripStatusLabel2.Text = "Программа работает";
            // 
            // toolStripProgressBar2
            // 
            this.toolStripProgressBar2.Name = "toolStripProgressBar2";
            this.toolStripProgressBar2.Size = new System.Drawing.Size(100, 16);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(424, 331);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.menuStrip2);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip2;
            this.Name = "Form1";
            this.Text = "Главная форма";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FormsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenForm1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenForm2ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem File1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Forms1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenForm11ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenForm21ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar2;
    }
}

